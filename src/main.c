#include "mem.h"
#include <stdio.h>

#define TEST_COUNT 4
#define HEAP_SIIZE 9112 

static struct block_header* get_header(const void * contents) {
    return (struct block_header *) ((uint8_t *) contents - offsetof(struct block_header, contents));
}


int main() {

    bool is_error = false;
    size_t test_number = 0;

    printf("----------------------------- Heap init-------------------------------\n");

    void* heap = heap_init(HEAP_SIIZE);
    printf("Finished successfully\n\n");

    printf("------------------------- Regular allocation -------------------------\n");

    const size_t block_size = 100;
    void* contents = _malloc(block_size);
    struct block_header* block_header = get_header(contents);

    if (block_header -> is_free) {
        printf("Error. Block shouldn't be free after allocation\n");
        is_error = true;
    }
    if (block_header -> capacity.bytes != block_size) {
        printf("Error. Block capacity doesn't match block_size\n");
        is_error = true;
    }

    debug_heap(stdout, heap);
    _free(contents);

    test_number++;
    if (!is_error) printf("Finished successfully\n\n");
    else {
        printf("Failed Test %zu of %d\n", test_number, TEST_COUNT);
        return 0;
    }


    printf("------------- Freeing one block when multiple allocated -------------\n");

    is_error = false;
    void* contents_left = _malloc(block_size);
    void* contents_middle = _malloc(block_size);
    void* contents_right = _malloc(block_size);

    debug_heap(stdout, heap);
    _free(contents_middle);
    debug_heap(stdout, heap);

    block_header = get_header(contents_middle);
    if (!block_header -> is_free) {
        printf("Error. Block should be free after deallocation\n");
        is_error = true;
    }
    if (block_header -> capacity.bytes != block_size) {
        printf("Error. Freeing middle block shouldn't corrupt its capacity\n");
        is_error = true;
    }

    test_number++;
    if (!is_error) printf("Finished successfully\n\n");
    else {
        printf("Failed Test %zu of %d\n", test_number, TEST_COUNT);
        return 0;
    }


    printf("--------------------- Freeing multiple blocks ---------------------\n");

    is_error = false;
    _free(contents_left);
    _free(contents_right);
    debug_heap(stdout, heap);
    block_header = get_header(contents_left);
    if (!block_header -> is_free) {
        printf("Error. Block should be free after deallocation\n");
        is_error = true;
    }
    block_header = get_header(contents_right);
    if (!block_header -> is_free) {
        printf("Error. Block should be free after deallocation\n");
        is_error = true;
    }
    test_number++;
    if (!is_error) printf("Finished successfully\n\n");
    else {
        printf("Failed Test %zu of %d\n", test_number, TEST_COUNT);
        return 0;
    }



    printf("--------------------- Out of memory, growing heap ---------------------\n");

    is_error = false;
    size_t large_block_size = 20 * block_size;
    void* contents_large = _malloc(large_block_size);
    block_header = get_header(contents_large);
    if (block_header -> is_free) {
        printf("Error. Block shouldn't be free after allocation\n");
        is_error = true;
    }
    if (block_header -> capacity.bytes != large_block_size) {
        printf("Error. Block capacity doesn't match block_size\n");
        is_error = true;
    }

    debug_heap(stdout, heap);
    test_number++;
    if (!is_error) printf("Finished successfully\n\n");
    else {
        printf("Failed Test %zu of %d\n", test_number, TEST_COUNT);
        return 0;
    }

    printf("All tests passed\n");

}
